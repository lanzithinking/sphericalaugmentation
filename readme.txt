Spherical Augmentation (SA) propose a general framework for efficiently handling 
constrained probability distributions ubiquitous in Bayesian Statistics.
Instead of letting the sampler bounce around in the constrained domain, SA
maps the constrained domain to a sphere in the augmented space.
By moving freely on the surface of this sphere, sampling algorithms 
handle constraints implicitly and generate proposals that remain within boundaries 
when mapped back to the original space.
SA generalizes the idea stemming from Spherical HMC to other applications, e.g. LMC
to handle a class of distributions on simplex.
SA provides a mathematically natural and computationally efficient framework,
and has been proven to be advantageous over state-of-the-art sampling algorithms, 
such as exact Hamiltonian Monte Carlo.

This repo contains R/Matlab/Python codes for examples in the following paper:

Shiwei Lan and Babak Shahbaba
Sampling Constrained Probability Distributions using Spherical Augmentation
Chapter 2 of Algorithmic Advances in Riemannian Geometry and Applications (to appear)
http://arxiv.org/abs/1506.05936

CopyRight: Shiwei Lan

Some of the Python codes are modified from codes of the following work:

Sam Patterson and Yee Whye Teh
Stochastic Gradient Riemannian Langevin Dynamics for Latent Dirichlet Allocation
In Advances in Neural Information Processing Systems, pages 3102–3110, 2013
http://www.stats.ox.ac.uk/~teh/sgrld.html

Please cite the references when using the codes, Thanks!

Shiwei Lan
9-18-2016
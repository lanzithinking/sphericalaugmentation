## This is to illustrate the bouncing in 1-norm constrained domain ##
# to explain WallHMC_diamond with pictures #

## one mode##
pdf('bounceindiamond.pdf',height=4,width=6)


op = par(mfrow=c(1,2),mar=c(1,1,2,1),oma=rep(0,4),mgp=c(0,0,0),pty='s')

## bounce in 1-norm constrained domain ##
# 1-norm constrained domain
curve(1-abs(x),-1,1,xlim=c(-1.25,1.25),ylim=c(-1.25,1.25),xlab='',ylab='',bty='n',xaxt='n',yaxt='n',lwd=3,col='red')
curve(-(1-abs(x)),-1,1,lwd=3,col='red',add=T)
arrows(-1.25,0,1.25,0,.15,lwd=2);arrows(0,-1.25,0,1.25,.15,lwd=2)
text(1.3,-.1,labels=expression(bold(theta)),cex=1.2)
#title(expression(q==1),cex.main=1.5)

# two points: one inside one outside
theta0=c(.6,.1);theta=c(-1.2,-.8)
points(theta0[1],theta0[2],pch=19); text(theta0[1]+.15,theta0[2]+.02,labels=expression(bold(theta[0])),cex=1.2)
points(theta[1],theta[2],pch=19); text(theta[1]+.12,theta[2]-.05,labels=expression(bold(theta)),cex=1.2)
lines(c(theta0[1],theta[1]),c(theta0[2],theta[2]),lwd=2)

# intersection at boundary
t=(-1-sum(theta0))/sum(theta-theta0)
theta1=theta0+(theta-theta0)*t
points(theta1[1],theta1[2],pch='X',cex=1.2,lwd=3); text(theta1[1]+.2,theta1[2]-.05,labels=expression(bold(theta[0]^"'")),cex=1.2)
# normal direction
n=rep(-1,2)
lines(theta1[1]+.4*c(-1,1)*n[1],theta1[2]+.4*c(-1,1)*n[2],lty=2);arrows(theta1[1],theta1[2],theta1[1]+.2*n[1],theta1[2]+.2*n[2],.1,lwd=2)
text(theta1[1]+.2*n[1]+.15,theta1[2]+.2*n[2],labels=expression(bold(n)),cex=1.2)
# bounces off to its mirror image about the boundary
thetan=theta-2*(t(n)%*%(theta-theta1))*n/2
lines(c(theta1[1],thetan[1]),c(theta1[2],thetan[2]),lwd=2,col='blue');
points(thetan[1],thetan[2],pch=19,col='blue'); text(thetan[1]+.12,thetan[2]+.05,labels=expression(bold(theta^"'")),cex=1.2,col='blue')

# intersection at coordinate planes
T=theta0/(theta0-theta)
thetac=theta0+(theta-theta0)%*%t(T)
points(thetac[1,],thetac[2,],pch=15,cex=1.2)


# find the hit point
plot(NULL,xlim=c(-1.25,1.25),ylim=c(-1.25,1.25),xlab='',ylab='',bty='n',xaxt='n',yaxt='n')
points(theta0[1],theta0[2],pch=19); text(theta0[1]+.15,theta0[2]+.02,labels=expression(bold(theta[0])),cex=1.2)
points(theta[1],theta[2],pch=19); text(theta[1]+.12,theta[2]-.05,labels=expression(bold(theta)),cex=1.2)
lines(c(theta0[1],theta[1]),c(theta0[2],theta[2]),lwd=2)
points(thetac[1,],thetac[2,],pch=15,cex=1.2);
theta2=theta0+(theta-theta0)*(t+.15)
points(c(theta1[1],theta2[1]),c(theta1[2],theta2[2]),pch=15,cex=1.2)
text(theta1[1]+.2,theta1[2]-.12,labels=expression(bold(theta[k-1])),cex=1.2);text(theta2[1]+.12,theta2[2]-.12,labels=expression(bold(theta[k])),cex=1.2)
perp=c((theta-theta0)[2],-(theta-theta0)[1]);perp=perp/sqrt(sum(perp^2))
thetam=(theta1+theta2)/2
lines(thetam[1]+.4*c(-1,1)*perp[1],thetam[2]+.4*c(-1,1)*perp[2],lty=2,lwd=2,col='red')
text(thetam[1]-.4*perp[1]+.3,thetam[2]-.4*perp[2]-.2,labels=expression(group('||',bold(theta),'||')[1]<=1),col='red',cex=1.1)

par(op)

dev.off()

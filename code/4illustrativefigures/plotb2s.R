## plot transformation from disk to sphere

library(scatterplot3d)

pdf('B2S.pdf',height=5,width=6)

op <- par(mfrow=c(1,2),mar=c(3,2,2,1) + 0.1,oma=rep(0,4),mgp=c(2,1,0),xpd=NA)
L = 72
s=seq(-pi,pi, length=L)

#### disk ####
z=c(rep(0,L) %*% t(cos(s)))
x=c(cos(s) %*% t(sin(s)))
y=c(sin(s) %*% t(sin(s)))

# shade
ball=scatterplot3d(x,y,z,color='gray',pch=19,cex.symbols=1.2,angle=90,xlim=c(-1,1),ylim=c(-1,1),zlim=c(-1,1),xlab='',ylab='',zlab='',scale.y=1.5,label=F,tick=F,box=F,grid=F,axis=F,mar=c(0,0,0,1) + 0.1)

# boundary
ball$points3d(cos(s),sin(s),rep(0,L),type='l',lwd=3,col='blue')

# axes
arrows(-2.5,3,2.5,3,.1,lwd=2); text(2.5,2.8,labels=expression(bold(theta)),cex=1.2)
arrows(0,-.5,0,6.5,.1,lwd=2)

# points
ball$points3d(-1/3,-2/3,0,pch=19)
ball$points3d(-1/3,-2/3,.15,pch='A')
ball$points3d(c(-1/3,0,1/3),c(-2/3,-1,-2/3),rep(0,3),type='l',lwd=2,lty=3)
ball$points3d(1/3,-2/3,0,pch=15)
ball$points3d(1/3,-2/3,.15,pch='B')
ball$points3d(0,-1,0,pch=4,lwd=3)

# notation
arrows(1.5,0,3.7,0,.1)
text(2.5,.25,labels=expression(paste(tilde(theta),'=(',theta,',',theta[D+1],')',sep='')))
text(2.5,-.25,labels=expression(paste(theta[D+1],'=(',1,'-','||',theta,'||',phantom()^2,')',phantom()^.5,sep='')))

#### sphere ####
hs=seq(0,pi/2, length=L)
z=c(rep(1,L) %*% t(cos(hs)))
x=c(cos(s) %*% t(sin(hs)))
y=c(sin(s) %*% t(sin(hs)))

# shade
sph=scatterplot3d(x,y,z,color='gray',pch=19,cex.symbols=1.2,angle=45,scale.y=.4,xlim=c(-1,1),ylim=c(-1,1),zlim=c(-1,1),xlab='',ylab='',zlab='',label=F,tick=F,box=F,grid=F,axis=F,mar=c(0,1,0,0) + 0.1)

# circle
sph$points3d(sin(s)*cos(pi/10),sin(s)*sin(pi/10),cos(s),type='l')

# boundary
sf=seq(-pi,pi/10,length=L/2)
sb=seq(-pi/10,pi,length=L/2)
sph$points3d(cos(sf),sin(sf),rep(0,L/2),type='l',lwd=3,col='blue')
sph$points3d(cos(sb),sin(sb),rep(0,L/2),type='l',lty=2,lwd=3,col='blue')

# circle
sr=seq(-pi/6,pi,length=L/2)
sl=seq(pi/6,pi,length=L/2)
sph$points3d(sin(sr)*cos(-pi/5),sin(sr)*sin(-pi/5),cos(sr),type='l')
sph$points3d(sin(sl)*cos(4*pi/5),sin(sl)*sin(4*pi/5),cos(sl),type='l',lty=2)

# axes
arrows(-2,.3,-1.6,.32,0,lwd=2); arrows(-1.6,.32,2.4,.48,0,lwd=2,lty=2); arrows(2.4,.48,3,.5,.1,lwd=2);
text(3,.4,labels=expression(bold(theta)),cex=1.2)
arrows(-.2,-.2,0,0,0,lwd=2); arrows(0,0,1,1,.1,lwd=2,lty=2);
arrows(.4,-1.8,.4,-1.6,0,lwd=2); arrows(.4,-1.6,.4,2.4,0,lwd=2,lty=2); arrows(.4,2.4,.4,2.7,.1,lwd=2);
text(.8,2.66,labels=expression(theta[D+1]))

# points
px=seq(-1/3,1/3,by=.1)
pz=-2*px
py=-sqrt(1-px^2-pz^2)
sph$points3d(px[1],py[1],pz[1],pch=19)
sph$points3d(px[1]+.13,py[1],pz[1],pch='A')
sph$points3d(px,py,pz,type='l',lwd=2,lty=3)
sph$points3d(px[length(px)],py[length(py)],pz[length(pz)],pch=15)
sph$points3d(px[length(px)]+.13,py[length(py)],pz[length(pz)],pch='B')
sph$points3d(0,-1,0,pch=4,lwd=3)

par(op)

dev.off()
## Wall HMC for box constraint ##

# inputs:
#   q_cur: initial state of q, lo<q<up
#   u_cur, du_cur: initial potential energy and its gradient
#   U: =-log(density(q)), potential function of q, or its gradient
#   lo, up: lower/upper limits of the state q
#   eps: step size
#   L: number of leapfrogs
# outputs:
#   q: new state
#   u, du: new potential energy and its gradient
#   Ind: proposal acceptance indicator

WallHMC = function (q_cur, u_cur, du_cur, U, lo, up, eps=.2, L=5){
#    browser()
	# initialization
	q = q_cur; D = length(q)
	u = u_cur; du = du_cur
    
    # sample velocity
    v = rnorm(D) # standard multi-normal
    
    # Evaluate potential and kinetic energies at start of trajectory
	E_cur = u + .5*sum(v^2)
	
	randL = ceiling(L*runif(1))
    
	wallhit = 0 # count wall hits
	
    # Alternate full steps for position and momentum
	# Make a half step for velocity
	v = v - eps/2 * du
    for (l in 1:randL){
	    
#	    # Make a half step for velocity
#	    v = v - eps/2 * du
	    
	    # Make a full step for the position
	    q = q + eps * v
		
		# handle constraints by wall hitting
		repeat{
			l_c = (q<lo); u_c = (q>up);
			
			if(any(l_c)){
				q[l_c] <- 2*lo[l_c] - q[l_c]
				v[l_c] <- -v[l_c]
				wallhit <- wallhit + 1
			} else if(any(u_c)){
				q[u_c] <- 2*up[u_c] - q[u_c]
				v[u_c] <- -v[u_c]
				wallhit <- wallhit + 1
			} else break
			
		}
		
	    # Make last half step for velocity
		# Make a full step for velocity
		du = U(q,d=T)
		if (l!=randL) v = v - eps * du
    }
	# Make last half step for velocity
	v = v - eps/2 * du
    
    # Evaluate potential and kinetic energies at end of trajectory
	u = U(q)
	E_prp = u + .5*sum(v^2)
    
    # Accept or reject the state at end of trajectory, returning either
    # the position at the end of the trajectory or the initial position
	logAP = -E_prp + E_cur
    
    if( is.finite(logAP)&&(log(runif(1))<min(0,logAP)) ) return (list(q = q, u = u, du = du, Ind = 1, wallhit = wallhit))
    else return (list(q = q_cur, u = u_cur, du = du_cur, Ind = 0, wallhit = wallhit))
    
}

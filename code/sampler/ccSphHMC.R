## Spherical HMC in Cartesian coordinate ##
# for general q-norm constraint #

# inputs:
#   q_cur: initial state of q_til, q_til in augmented space with |q_til|=1
#   u_cur, du_cur: initial potential energy and its gradient
#   U: =-log(density(q)), potential function of q (|q|<1), or its gradient
#   eps: step size
#   L: number of leapfrogs
# outputs:
#   q: new state
#   u, du: new potential energy and its gradient
#   Ind: proposal acceptance indicator

SphHMC = function (q_cur, u_cur, du_cur, U, eps=.2, L=5){
#    browser()
    # initialization
    q = q_cur; D = length(q)
	u = u_cur; du = du_cur
    
    # sample velocity
    v = rnorm(D) # standard multi-normal
    v <- v - q*(t(q)%*%v) # force v to lie in tangent space of sphere
    
    # Evaluate potential and kinetic energies at start of trajectory
    E_cur = u + .5*sum(v^2)
	
	randL = ceiling(L*runif(1))
    
    # Alternate full steps for position and momentum
	# Make a half step for velocity
	g = c(du,0) - q*(t(q[-D])%*%du)
	v = v - eps/2 * g
    for (l in 1:randL){
	    
#	    # Make a half step for velocity
#		g = c(du,0) - q*(t(q[-D])%*%du)
#	    v = v - eps/2 * g
	    
	    # Make a full step for the position
	    q0 = q; v_nom = sqrt(sum(v^2))
		cosvt = cos(v_nom*eps); sinvt=sin(v_nom*eps)
	    q = q0*cosvt + v/v_nom*sinvt
	    v <- -q0*v_nom*sinvt + v*cosvt
	    
	    # Make last half step for velocity
		# Make a full step for velocity
		du = U(q[-D],d=T); g = c(du,0) - q*(t(q[-D])%*%du)
		if (l!=randL) v = v - eps * g
		
#		if (abs(t(q)%*%v)>1e-6){ # calibrate direction possibly deviated by error accumulation 
#			v <- v - q*(t(q)%*%v)
#			cat('Direction calibrated!')
#		}
    }
	# Make last half step for velocity
	v = v - eps/2 * g
    
    # Evaluate potential and kinetic energies at end of trajectory
	u = U(q[-D])
    E_prp = u + .5*sum(v^2)
    
    # Accept or reject the state at end of trajectory, returning either
    # the position at the end of the trajectory or the initial position
    logAP = -E_prp + E_cur
    
    if( is.finite(logAP)&&(log(runif(1))<min(0,logAP)) ) return (list(q = q, u = u, du = du, Ind = 1))
    else return (list(q = q_cur, u = u_cur, du = du_cur, Ind = 0))
    
}

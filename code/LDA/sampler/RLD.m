%%%% Riemannian Langevin dynamics with 'expanded-mean' parametrization %%%%
% by Sam Patterson and Yee Whye Teh %
% for simplex constraint %
% Note: this is not a generic algorithm: it is for invG=diag(abs(q)) %

% inputs:
%   q_cur: initial state of q_til, q_til in augmented space with |q_til|=1
%   u_cur, du_cur: initial potential energy and its gradient
%   U: =-log(density(q)), potential function of q (|q|<1), or its gradient
%   eps: step size
%   L: number of leapfrogs
% outputs:
%   q: new state
%   u, du: new potential energy and its gradient
%   acpt: proposal acceptance indicator

% This is can be recognized as one-step lite LMC %


function [q, u, du, acpt] = RLD(q_cur, u_cur, du_cur, U, eps)

% initialization
q = q_cur; D = length(q);
u = u_cur; du = du_cur;

% sample velocity
z = randn(D,1);
v = sqrt(abs(q)).*z; % v ~ N(0,invG), invG = diag(abs(q)), not sure about the correctness

% current energy
E_cur = u + (z'*z)/2;% + .5*sum(log(abs(q)));

% forward half step of velocity
mu = -abs(q).*du + sign(q);
v = v + eps/2.*mu;

% full step of position
q = q + eps.*v;

% backward half step of velocity
du = U(q,1);
mu = -abs(q).*du + sign(q);
v = v + eps/2.*mu;

% new energy
u = U(q);
E_prp = u + ((v./abs(q))'*v)/2;% + .5*sum(log(abs(q)));

% log of Metropolis ratio
logAP = -E_prp + E_cur;

% accept or reject the proposal at end of trajectory
if isfinite(logAP) && (log(rand) < min([0,logAP]))
    acpt = 1;
else
    q = q_cur; u = u_cur; du = du_cur;
    acpt = 0;
end

end
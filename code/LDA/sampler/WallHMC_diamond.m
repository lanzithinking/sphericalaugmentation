%%%% Wall HMC for diamond constraint (l1 norm less than 1) %%%%
% for simplex constraint %

% inputs:
%   q_cur: initial state of q, |q|<1
%   u_cur, du_cur: initial potential energy and its gradient
%   U: =-log(density(q)), potential function of q (|q|<1), or its gradient
%   eps: step size
%   L: number of leapfrogs
% outputs:
%   q: new state
%   u, du: new potential energy and its gradient
%   acpt: proposal acceptance indicator

function [q, u, du, acpt, wallhit] = WallHMC_diamond(q_cur, u_cur, du_cur, U, eps, L)
if nargin<6
    L=1;
end

% initialization
q = q_cur; D = length(q);
u = u_cur; du = du_cur;

% sample velocity
v = randn(D,1);

% current energy
E_cur = u + (v'*v)/2;

% set random integration steps
randL=ceil(rand*L);

wallhit = 0; % count wall hits

% forward half step of velocity
v = v - eps/2.*du;
for l = 1:randL
    
    % full step evolution of position
    q0 = q; % assume q0 is within diamond constraint
    q = q + eps.*v;
    
    hit = 0;
    % handle constraints by wall hitting
    while sum(abs(q))>1
        if all(q0.*q>=0)
            ndirxn = sign(q0);
        else
            no0_idx = (q-q0~=0);
            xT = q0(no0_idx)./(q0(no0_idx)-q(no0_idx)); % coordinate plane intersecting times
            xT = sort(xT(xT>=0&xT<=1)); % keep those between 0 and 1
            xT = [0;xT;1]; % add starting and ending time
            intsct = repmat(q0,1,length(xT)) + (q-q0)*xT'; intsct(abs(intsct)<1e-10)=0; % intersection with coordinate planes
            firstoff = 1+find(sum(abs(intsct(:,2:end)))>1,1); % determine the first intersection point outside the diamond
            ndirxn = sign(sum(sign(intsct(:,firstoff-1:firstoff)),2)); % normal direction of the piece of boundary being hit
        end
        hitime = (1-ndirxn'*q0)/(ndirxn'*(q-q0)); % hitting time
        q0 = q0 + (q-q0)*hitime; % hit point
        q = q - 2*ndirxn*(ndirxn'*q-1)/D; % mirror image of q about that piece of boundary
%         q = q - 2*ndirxn*(ndirxn'*(q-q0))/sum(abs(ndirxn)); % may be more accurate
        hit = 1;
        wallhit = wallhit + 1;
    end
    if hit
        v = (q-q0)*sqrt(v'*v/sum((q-q0).^2)); % adjust the velocity direction if hit!!
    end
    
    % backward full step of velocity
    du = U(q,1);
    if l~=randL
        v = v - eps.*du;
    end
end
% backward last half step of velocity
v = v - eps/2.*du;

% new energy
u = U(q);
E_prp = u + (v'*v)/2;

% log of Metropolis ratio
logAP = -E_prp + E_cur;

% accept or reject the proposal at end of trajectory
if isfinite(logAP) && (log(rand) < min([0,logAP]))
    acpt = 1;
else
    q = q_cur; u = u_cur; du = du_cur;
    acpt = 0;
end

end
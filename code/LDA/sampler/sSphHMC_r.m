%%%% Spherical HMC in spherical coordinate with radius r %%%%
% for box type (infinity norm) constraint %

% inputs:
%   q_cur: initial state of q, q in hyper-rectangle (0,pi)^(D-1)*(0,2*pi)
%   u_cur, du_cur: initial potential energy and its gradient
%   U: =-log(density(q)), potential function of q, or its gradient
%   r: radius of sphere
%   eps: step size
%   L: number of leapfrogs
% outputs:
%   q: new state
%   u, du: new potential energy and its gradient
%   acpt: proposal acceptance indicator

function [q, u, du, acpt] = sSphHMC_r(q_cur, u_cur, du_cur, U, r, eps, L)
if nargin<7
    L=1;
end

% initialization
q = q_cur; D = length(q);
u = u_cur; du = du_cur;

% sample velocity
z = randn(D,1);
cumlogsinq = cumsum(log(abs(sin(q))));
v = sign(z).*exp(log(abs(z))-[0;cumlogsinq(1:end-1)]-log(r));

% current energy
E_cur = u + (z'*z)/2;% - sum(cumlogsinq(1:end-1));

% set random integration steps
randL=ceil(rand*L);

% forward half step of velocity
v = v - .5*sign(du).*exp(log(abs(du))-2*[0;cumlogsinq(1:end-1)]+(1:D)'.*log(eps)-2*log(r));
for l = 1:randL
    
    % full step evolution on sphere along great circle
    % 1. map to augment unit sphere
    cosq = cos(q);
    q_s = sign([cosq;sin(q(end))]).*exp([log(abs(cosq));0]+[0;cumlogsinq]);
    v_s = ( [-v.*tan(q);0] + [0;cumsum(v.*cot(q))] ).* q_s;
    % 2. rotate on sphere
    q_s0 = q_s; v_nom = sqrt(v_s'*v_s);
    cosvt = cos(eps*v_nom); sinvt = sin(eps*v_nom);
    q_s = q_s0.*cosvt + v_s/v_nom.*sinvt;
    v_s = -q_s0*v_nom.*sinvt + v_s.*cosvt;
    % 3. map back to hyper-rectangle
    cumqs2 = cumsum(q_s.^2); cotq = q_s(1:end-1)./sqrt(1-cumqs2(1:end-1));
    q = pi/2-atan(cotq); % the range of acot in Matlab is (-pi/2,pi/2), not the standard (0,pi)!
    q(end) = pi + sign(q_s(end))*(q(end)-pi);
    cumqvs = cumsum(q_s.*v_s);
    v = -cotq.*(v_s(1:end-1)./q_s(1:end-1)+[0;cumqvs(1:(D-1))]./(1-[0;cumqs2(1:(D-1))])); v(end) = v(end)*sign(q_s(end));
    
    % backward full step of velocity
    cumlogsinq = cumsum(log(abs(sin(q))));
    du = U(q,1);
    if l~=randL
        v = v - sign(du).*exp(log(abs(du))-2*[0;cumlogsinq(1:end-1)]+(1:D)'.*log(eps)-2*log(r));
    end
    
end
% backward last half step of velocity
z = sign(v).*exp(log(abs(v))+[0;cumlogsinq(1:end-1)]) - .5*sign(du).*exp(log(abs(du))-[0;cumlogsinq(1:end-1)]+(1:D)'.*log(eps)-log(r));

% new energy
u = U(q);
E_prp = u + (z'*z)/2;% - sum(cumlogsinq(1:end-1));

% log of Metropolis ratio
logAP = -E_prp + E_cur;

% accept or reject the proposal at end of trajectory
if isfinite(logAP) && (log(rand) < min([0,logAP]))
    acpt = 1;
else
    q = q_cur; u = u_cur; du = du_cur;
    acpt = 0;
end

end
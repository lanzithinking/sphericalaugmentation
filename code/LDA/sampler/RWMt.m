%%%% Random Walk Metropolis with truncation %%%%
% for simplex constraint %

% inputs:
%   q_cur: initial state of q in simplex
%   u_cur: initial potential energy
%   U: =-log(density(q)), potential function of q
%   eps: step size
% outputs:
%   q: new state
%   u: new potential energy
%   acpt: proposal acceptance indicator
%   violation: constraint violation

function [q, u, acpt, violation] = RWMt(q_cur, u_cur, U, eps)
% initialization
q = q_cur; D = length(q);
% u = u_cur;

% sample velocity
z = randn(D,1);

% evolve position using random direction
q = q + eps.*z;

% new energy
u = U(q);

% log of Metropolis ratio
logAP = -u + u_cur;

% accept or reject the proposal at end of trajectory
if isfinite(logAP) && (log(rand) < min([0,logAP]))
    if all(q>=0&q<=1)&&sum(q)<=1
        acpt = 1; violation = 0;
    else
        q = q_cur; u = u_cur;
        acpt = 0; violation = 1; % to count number of rejections due to constraint violation
    end
else
    q = q_cur; u = u_cur;
    acpt = 0; violation = 0;
end

end
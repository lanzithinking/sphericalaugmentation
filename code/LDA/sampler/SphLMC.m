%%%% Spherical LMC with Fisher metric %%%%
% for simplex constraint %

% inputs:
%   q_cur: initial state of q_til, q_til in augmented space with |q_til|=1
%   u_cur, du_cur: initial potential energy and its gradient
%   U: =-log(density(q)), potential function of q (|q|<1), or its gradient
%   N: number of total counts, factor to adjust canonical spherical metric
%   eps: step size
%   L: number of leapfrogs
% outputs:
%   q: new state
%   u, du: new potential energy and its gradient
%   acpt: proposal acceptance indicator

function [q, u, du, acpt] = SphLMC(q_cur, u_cur, du_cur, U, N, eps, L)
if nargin<7
    L=1;
end

% initialization
q = q_cur; D = length(q);
u = u_cur; du = du_cur;

% sample velocity
v = randn(D,1);
v = v - q*(q'*v); % project to tangent plane
v = v/(2*sqrt(N)); % adjust with the right metric

% current energy
E_cur = u + (v'*v)*2*N; % adjust with the right metric

% set random integration steps
randL=ceil(rand*L);

% forward half step of velocity
g = ([du;0] - q*(q(1:end-1)'*du))/(4*N);
v = v - eps/2.*g;
for l = 1:randL
    
    % full step evolution on sphere along great circle
    q0 = q; v_nom = sqrt(v'*v);
    cosvt = cos(eps*v_nom); sinvt = sin(eps*v_nom);
    q = q0.*cosvt + v/v_nom.*sinvt;
    v = -q0*v_nom.*sinvt + v.*cosvt;
    
    % backward full step of velocity
    du = U(q(1:end-1),1); g = ([du;0] - q*(q(1:end-1)'*du))/(4*N);
    if l~=randL
        v = v - eps.*g;
    end
    
    % calibrate direction possibly deviated by error accumulation
    if abs(q'*v)>1e-6
        v = v - q*(q'*v);
%         disp('Direction calibrated!');
    end
    
end
% backward last half step of velocity
v = v - eps/2.*g;

% new energy
u = U(q(1:end-1));
E_prp = u + (v'*v)*2*N;

% log of Metropolis ratio
logAP = -E_prp + E_cur;

% accept or reject the proposal at end of trajectory
if isfinite(logAP) && (log(rand) < min([0,logAP]))
    acpt = 1;
else
    q = q_cur; u = u_cur; du = du_cur;
    acpt = 0;
end

end
% simulation of multinomial/dirichlet distribution using Spherical LMC

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);


% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=.5; N=sum(n); e=1;

% smallest potential energy
% options=optimoptions('fmincon','GradObj','on','display','iter');
% [theta_hat,u_min]=fmincon(@(theta)UdU(theta,n,alpha),ones(D,1)./D,[],[],ones(1,D),1,zeros(D,1),ones(D,1),[],options);

% sampling setting
stepsz=1.2e0; Nleap=1;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=1;
samp=zeros(Nsamp-NBurnIn,D-1);
acpt=0; % overall acceptance
accp=0; % online acceptance
wt=zeros(Nsamp-NBurnIn,1);

% initializatioin
p=rand(D,1); p=p./sum(p);
% p=(n+alpha)/sum(n+alpha);
theta_til=sqrt(p);
theta=theta_til(1:end-1);
% u=Ut(theta,n,alpha); du=Ut(theta,n,alpha,1);
u=Ut_all(theta,n,alpha); du=Ut_all(theta,n,alpha,1);
% theta_til=p;
% theta=theta_til(1:end-1);
% u=U(theta,n,alpha); du=U(theta,n,alpha,1);

disp('Running Spherical LMC...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use Spherical LMC to get sample theta
%     [theta_til,u,du,acpt_ind]=SphLMC(theta_til,u,du,@(theta,der)Ut(theta,n,alpha,(nargin==2)),N,stepsz,Nleap);
%     [theta_til,u,du,acpt_ind]=SphLMC_diagmet(theta_til,u,du,@(theta,der)Ut(theta,n,alpha,(nargin==2)),stepsz,Nleap);
%     [theta_til,u,du,acpt_ind]=SphLMC_mF(theta_til,u,du,@(theta,der)Ut(theta,n,alpha,(nargin==2)),e,stepsz,Nleap);
%     [theta_til,u,du,acpt_ind]=SphLMC_circ(theta_til,u,du,@(theta,der)U(theta,n,alpha,(nargin==2)),N,stepsz,Nleap); % no need to resample with weights
    [theta_til,u,du,acpt_ind]=SphLMC(theta_til,u,du,@(theta,der)Ut_all(theta,n,alpha,(nargin==2)),N,stepsz,Nleap); % no need to resample with weights
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        theta=theta_til(1:end-1);
        p=theta.^2;
        samp(iter-NBurnIn,:)=p;
        acpt=acpt+acpt_ind;
%         wt(iter-NBurnIn)=sum(log(abs(theta_til))) + (D-1)*log(2); % log weights
    end

end

% save result
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
wt=wt(1:thin:(Nsamp-NBurnIn));
wt=exp(wt-median(wt)); % recover weights
% resample
resamp_idx=datasample(1:size(samp,1),floor((Nsamp-NBurnIn)/thin),'replace',true,'weights',wt);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_SphLMC_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','Nleap','samp','wt','resamp_idx','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

% transformed Energy function (with log Jacobian derminant) on unit ball |theta|<1 %
% call user defined energy function U %
% no need to resample with weights %

function [output]=Ut_all(theta,n,alpha,der)
if(nargin<4)
    der=0;
end
% transform back to original domain (simplex)
p = theta.^2;
theta_aug = sqrt(1-theta'*theta);

if der==0
    u = U(p,n,alpha);
    logdetJ = sum(log(abs([theta;theta_aug])));
    ut = u - logdetJ;
    output = ut;
elseif der==1
    du = U(p,n,alpha,1);
    dut = du.*2.*theta;
    dlogdetJ = 1./theta - theta./theta_aug^2;
    dut = dut - dlogdetJ;
    output = dut;
else
    disp('wrong choice of der!');
end

end
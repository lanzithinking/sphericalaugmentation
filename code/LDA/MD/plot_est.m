% Plot estimates %

addpath('./summary/');
% algorithms
alg={'RWMt','WallHMC','RLD','SphLMC'};
Nalg=length(alg);

% truth
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=.5;
mean_truth=(n(1:D-1)'+alpha)/sum(n+alpha);

D=9;
% set figure
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
set(fig1,'pos',[0 800 800 600]);
hold on;

% access files
files=dir(['./summary/','*.mat']);
nfiles=length(files);
% plot estimates
for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j).name,['_',alg{i},'_']))
            load(strcat('./summary/',files(j).name));
            if ~isempty(strfind(alg{i},'Sph'))
                MEAN=mean(samp(resamp_idx,:));
                STD=std(samp(resamp_idx,:));
            else
                MEAN=mean(samp(:,1:D));
                STD=std(samp(:,1:D));
            end
            break;
        end
    end
    MEAN=MEAN-mean_truth; % adjust it for comparison
    errorbar((1:D)+(i-1)*.12,MEAN,STD,'X','linewidth',1.5,'markersize',10);
end
box on;
set(gca,'fontsize',12);
xlabel('Category','fontsize',15); ylabel('Probability estimates adjusted by the true values','fontsize',15);

% add truth
plot([0,D+1],zeros(1,2),'r-','linewidth',1.5);

% add legend
lg=legend({'RWM',alg{2:end}},'Truth');
set(lg,'fontsize',15,'location','best');
% Energy function of MD distribution in expanded-mean parametrization %

function [output]=U_em(theta,n,alpha,der)
if(nargin<4)
    der=0;
end

NM1_theta = sum(abs(theta));

if der==0
%     loglik = n'*(log(abs(theta))-log(NM1_theta));
%     logpri = (alpha-1)'*log(abs(theta))-NM1_theta;
    u = -( (n+alpha-1)'*log(abs(theta)) - sum(n)*log(NM1_theta) - NM1_theta );
    output = u;
elseif der==1
%     dloglik = n./theta - sign(theta)*sum(n)/NM1_theta;
%     dlogpri = (alpha-1)./theta - sign(theta);
    du = -( (n+alpha-1)./theta - sign(theta).*(sum(n)/NM1_theta+1) );
    output = du;
else
    disp('wrong choice of der!');
end

end
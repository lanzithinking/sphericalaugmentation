% Transformation from unit cube to simplex %

function[p]=c2x(z)
if any(z<0)||any(z>1)
    warning('Check the definition domain!');
end
cumlog1mz=cumsum(log(1-z));
p=exp([log(z);0]+[0;cumlog1mz]);
end


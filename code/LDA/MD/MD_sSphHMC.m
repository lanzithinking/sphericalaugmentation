% simulation of multinomial/dirichlet distribution using s-SphHMC
% simplex is transformed to infinity norm constrained domain

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);


% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=1; r=1.01;

% sampling setting
stepsz=1e-1; Nleap=1;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=100;
samp=zeros(Nsamp-NBurnIn,D);
acpt=0; % overall acceptance
accp=0; % online acceptance
wt=zeros(Nsamp-NBurnIn,1);

% initializatioin
p=rand(D,1); p=p./sum(p);
x=sqrt(p);
theta=c2s(x);
u=Us_full(theta,n,alpha); du=Us_full(theta,n,alpha,1);

disp('Running Spherical HMC on stereographical coordinate...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use Spherical HMC to get sample theta
%     [theta,u,du,acpt_ind]=sSphHMC(theta,u,du,@(theta,der)Us_full(theta,n,alpha,(nargin==2)),stepsz,Nleap);
    [theta,u,du,acpt_ind]=sSphHMC_r(theta,u,du,@(theta,der)Us_full(theta,n,alpha,(nargin==2)),r,stepsz,Nleap);
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        theta=theta/2;theta(end)=theta(end)/2;
        x=s2c(theta);
        samp(iter-NBurnIn,:)=x.^2;
        acpt=acpt+acpt_ind;
        wt(iter-NBurnIn)=2*sum(log(x(1:end-1))) + sum(log(tan(theta))) - D*log(2); % log weights
    end

end

% save result
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
wt=wt(1:thin:(Nsamp-NBurnIn));
wt=exp(wt-median(wt)); % recover weights
% resample
resamp_idx=datasample(1:size(samp,1),floor((Nsamp-NBurnIn)/thin),'replace',true,'weights',wt);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_sSphHMC_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','Nleap','samp','wt','resamp_idx','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

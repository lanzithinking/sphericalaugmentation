% this is to summarize the estimates by different algorithms.

addpath('./summary/');
% algorithms
alg={'RWMt','WallHMC','RLD','SphHMC','SphLMC'};
Nalg=length(alg);
% store estimates
D=9;
MEAN=zeros(Nalg,D);
STD=zeros(Nalg,D);

% access files
files=dir(['./summary/','*.mat']);
nfiles=length(files);
% compute estimates
for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j).name,['_',alg{i},'_']))
            load(strcat('./summary/',files(j).name));
            if ~isempty(strfind(alg{i},'Sph'))
                MEAN(i,:)=mean(samp(resamp_idx,:));
                STD(i,:)=std(samp(resamp_idx,:));
            else
                MEAN(i,:)=mean(samp(:,1:D));
                STD(i,:)=std(samp(:,1:D));
            end
            break;
        end
    end
end
% make table
T = table(MEAN,STD,'rownames',alg);

% truth
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=1;
mean_truth=(n+alpha)/sum(n+alpha);
Truth = {mean_truth(1:D-1)',zeros(1,D-1)};
Truth = cell2table(Truth,'rownames',{'Truth'});
Truth.Properties.VariableNames = T.Properties.VariableNames;

% summarize in table
T = [Truth;T];
writetable(T,'./summary/sumry_est.txt','WriteRowNames',true);


% Plot autocorrelation function of samples %

addpath('./result/');
addpath('./summary/');
% algorithms
alg={'RWMt','WallHMC','RLD','SphLMC'};
Nalg=length(alg);

dim=[1,2,9];
L_dim=length(dim);

% set figure
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
set(fig1,'pos',[0 800 900 600]);
hold on;

% access files
files=dir(['./summary/','*.mat']);
nfiles=length(files);
% plot
for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j).name,['_',alg{i},'_']))
            load(strcat('./summary/',files(j).name),'samp');
            for d=1:L_dim
                subplot(L_dim,Nalg,sub2ind([Nalg,L_dim],i,d)); hold on;
                autocorr(samp(:,d),20);
                set(gca,'fontsize',12);
                ylabel(['Autocorrelation of p_',num2str(dim(d))],'fontsize',15);
                if d==1
                    if i==1
                        title('RWM','fontsize',18);
                    else
                        title(alg{i},'fontsize',18);
                    end
                else
                    title('');
                end
            end
        end
    end
end
                
        
        
% simulation of multinomial/dirichlet distribution using RLD with metropolis
% adjustment

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=.5;

% sampling setting
stepsz=5e-2;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=1;
samp=zeros(Nsamp-NBurnIn,D);
acpt=0; % overall acceptance
accp=0; % online acceptance

% initializatioin
theta=rand(D,1);
u=U_em(theta,n,alpha); du=U_em(theta,n,alpha,1);

disp('Running Riemannian Langevin dynamics...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use RLD to get sample theta
    [theta,u,du,acpt_ind]=RLD(theta,u,du,@(theta,der)U_em(theta,n,alpha,(nargin==2)),stepsz);
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        NM1_theta=sum(abs(theta));
        samp(iter-NBurnIn,:)=abs(theta)/NM1_theta;
        acpt=acpt+acpt_ind;
    end

end

% save result
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_RLD_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','samp','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

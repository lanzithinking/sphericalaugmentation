% simulation of multinomial/dirichlet distribution using RWMt

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);


% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=.5;

% sampling setting
stepsz=2e-4;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=1;
samp=zeros(Nsamp-NBurnIn,D-1);
acpt=0; % overall acceptance
accp=0; % online acceptance
offbdy = 0; % count of accepted proposals off the boundary

% initializatioin
p=rand(D,1); p=p./sum(p);
p(end)=[];
u=U(p,n,alpha);

disp('Running Random Walk Metropolis with truncation...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use RWMt to get sample
    [p,u,acpt_ind,off_ind]=RWMt(p,u,@(p)U(p,n,alpha),stepsz);
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        samp(iter-NBurnIn,:)=p;
        acpt=acpt+acpt_ind;
        offbdy = offbdy + off_ind;
    end

end

% save result
time=toc;
offrate=offbdy/(Nsamp-NBurnIn-acpt);
acpt=acpt/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_RWMt_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','samp','offrate','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
disp(['The off-boundary rate: ',num2str(offrate)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

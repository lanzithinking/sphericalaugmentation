% transformed extended Energy function on unit ball |theta|<1 %
% call user defined energy function U %

function [output]=Phit(theta,n,alpha,der)
if(nargin<4)
    der=0;
end
% transform back to original domain (simplex)
p = theta.^2; theta_end2 = 1-sum(p);

if der==0
    ut = U(p,n,alpha); logrtG = -.5*log(theta_end2);
    phit = ut - logrtG;
    output = phit;
elseif der==1
    du = U(p,n,alpha,1);
    dut = du.*2.*theta; dlogrtG = theta/sqrt(theta_end2);
    dphit = dut + dlogrtG;
    output = dphit;
else
    disp('wrong choice of der!');
end

end
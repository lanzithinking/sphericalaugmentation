% Energy function of MD distribution %
% user defined. p is one less than number ofcategories %

function [output]=U(p,n,alpha,der)
if(nargin<4)
    der=0;
end

% probability of the last category
p_end = 1-sum(p);

if der==0
%     loglik = n'*log([p;p_end]);
%     logpri = (alpha-1)'*log([p;p_end]);
    u = -(n+alpha-1)'*log([p;p_end]);
    output = u;
elseif der==1
%     dloglik = n(1:end-1)./p-n(end)/p_end;
%     dlogpri = (alpha(1:end-1)-1)./p-(alpha(end)-1)/p_end;
    n_til = n+alpha-1;
    du = -(n_til(1:end-1)./p-n_til(end)/p_end);
    output = du;
else
    disp('wrong choice of der!');
end

end
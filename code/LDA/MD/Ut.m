% transformed Energy function on unit ball |theta|<1 %
% call user defined energy function U %

function [output]=Ut(theta,n,alpha,der)
if(nargin<4)
    der=0;
end
% transform back to original domain (simplex)
p = theta.^2;

if der==0
    ut = U(p,n,alpha);
    output = ut;
elseif der==1
    du = U(p,n,alpha,1);
    dut = du.*2.*theta;
    output = dut;
else
    disp('wrong choice of der!');
end

end
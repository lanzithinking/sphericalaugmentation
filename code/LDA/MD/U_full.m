% Energy function of MD distribution %
% user defined. p is the full probability vector %

function [output]=U_full(p,n,alpha,der)
if(nargin<4)
    der=0;
end


if der==0
%     loglik = n'*log(p);
%     logpri = (alpha-1)'*log(p);
    u = -(n+alpha-1)'*log(p);
    output = u;
elseif der==1
%     dloglik = n./p;
%     dlogpri = (alpha-1)./p;
    du = -(n+alpha-1)./p;
    output = du;
else
    disp('wrong choice of der!');
end

end
% transformed Energy function on unit ball |theta|<1 via cubic transform %
% call user defined energy function U %

function [output]=Uc_full(theta,n,alpha,der)
if(nargin<4)
    der=0;
end
nom2_theta = theta'*theta;
nom2oinf = sqrt(nom2_theta)/max(abs(theta)); % ratio of 2 norm vs infinite norm of theta
% tansform to unit cube
x_c = theta.*nom2oinf;
z = (x_c+1)/2;
% transform back to original domain (simplex)
p = c2x(z);

if der==0
    ut = U_full(p,n,alpha);
    output = ut;
elseif der==1
%     du = U_full(p,n,alpha,1);
%     dut = p.*du;
    dut = -(n+alpha-1);
    cumdut = cumsum(dut);
    dut = dut(1:end-1)./z - (sum(dut)-cumdut(1:end-1))./(1-z);
    dut = dut/2;
    [~,max_id] = max(abs(theta));
    thetamnom = x_c/nom2_theta; thetamnom(max_id) = thetamnom(max_id) - nom2oinf/theta(max_id);
    dut = dut*nom2oinf + thetamnom *(theta'*dut);
    output = dut;
else
    disp('wrong choice of der!');
end

end
% transformed Energy function on unit sphere in spherical coordinate %
% call user defined energy function U %

function [output]=Us_full(theta,n,alpha,der)
if(nargin<4)
    der=0;
end
% scale to the first quadrant
theta = theta/2; theta(end) = theta(end)/2;
% transform to cartesian coordinate
x = s2c(theta);
% transform back to original domain (simplex)
p = x.^2;

if der==0
    ut = U_full(p,n,alpha);
    output = ut;
elseif der==1
    du = U_full(p,n,alpha,1);
    dut = 2*p.*du; cumdut = cumsum(dut);
    dut = -tan(theta).*dut(1:end-1) + cot(theta).*(sum(dut)-cumdut(1:end-1));
    dut = dut/2; dut(end) = dut(end)/2;
    output = dut;
else
    disp('wrong choice of der!');
end

end
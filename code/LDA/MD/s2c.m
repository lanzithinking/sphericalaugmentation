% Transformation from spherical coordinate to Cartesian coordinate %

function[x]=s2c(theta)
if(any(theta<0)||any(theta(1:end-1)>pi)||theta(end)>2*pi)
    warning('Check the definition domain!');
end
cumlogsintheta=cumsum(log(abs(sin(theta))));
costheta=cos(theta);
x=sign([costheta;sin(theta(end))]).*exp([log(abs(costheta));0]+[0;cumlogsintheta]);
end


% simulation of multinomial/dirichlet distribution using Wall HMC

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);


% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=.5;

% sampling setting
stepsz=3e-4; Nleap=1;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=1;
samp=zeros(Nsamp-NBurnIn,D-1);
acpt=0; % overall acceptance
accp=0; % online acceptance
wallhits = 0; % count the number of Wall hitting

% initializatioin
p=rand(D,1); p=p./sum(p);
theta=p(1:end-1);
u=U(theta,n,alpha); du=U(theta,n,alpha,1);

disp('Running Wall HMC...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use Wall HMC to get sample theta
    [theta,u,du,acpt_ind,Nhits]=WallHMC_diamond(theta,u,du,@(theta,der)U(abs(theta),n,alpha,(nargin==2)),stepsz,Nleap);
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        samp(iter-NBurnIn,:)=abs(theta);
        acpt=acpt+acpt_ind;
        wallhits = wallhits + Nhits;
    end

end

% save result
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
hitrate = wallhits/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_WallHMC_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','Nleap','samp','hitrate','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
disp(['The wall hitting rate: ',num2str(hitrate)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

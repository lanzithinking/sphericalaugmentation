% Transformation from Cartesian coordinate to spherical coordinate %

function[theta]=c2s(x)
if abs(x'*x-1)>1e-6
    warning('Not on the sphere!');
end
cumx2=cumsum(x.^2);
cottheta=x(1:end-1)./sqrt(1-cumx2(1:end-1));
theta=acot(cottheta); theta(end)=pi+sign(x(end))*(theta(end)-pi);
end


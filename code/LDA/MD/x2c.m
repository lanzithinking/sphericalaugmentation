% Transformation from simplex to unit cube %

function[z]=x2c(p)
if any(p<0)||any(p>1)||abs(sum(p)-1)>1e-6
    warning('Not on the simplex!');
end
cump=cumsum(p);
z=p(1:end-1)./(1-[0;cump(1:end-2)]);
end


% simulation of multinomial/dirichlet distribution using c-SphHMC

clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);


% simulate data
D=10;% N=1000;
n=zeros(D,1); n(1:3)=[90,5,5];
% x=[];
% for i=1:D
%     x=[x;i*ones(n(i),1)];
% end
% x=[x;zeros(N-length(x),1)];
% x=x(randperm(N));
alpha=1;

% sampling setting
stepsz=6e-2; Nleap=1;

% allocation to save
Nsamp=110000; NBurnIn=10000; thin=1;
samp=zeros(Nsamp-NBurnIn,D-1);
% samp=zeros(Nsamp-NBurnIn,D);
acpt=0; % overall acceptance
accp=0; % online acceptance
wt=zeros(Nsamp-NBurnIn,1);

% initializatioin
p=rand(D,1); p=p./sum(p);
theta_til=sqrt(p);
theta=theta_til(1:end-1);
u=Ut(theta,n,alpha); du=Ut(theta,n,alpha,1);
% u=Uc_full(theta,n,alpha); du=Uc_full(theta,n,alpha,1);

disp('Running Spherical HMC...');
tic;
for iter=1:Nsamp
    
    % display online acceptance rate per 100 iteration
    if(mod(iter,1000)==0)
        disp(['Iteration ',num2str(iter),' completed!']);
        disp(['Acceptance Rate: ', num2str(accp/1000)]);
        accp=0;
    end
    
    % Use Spherical HMC to get sample theta
    [theta_til,u,du,acpt_ind]=cSphHMC(theta_til,u,du,@(theta,der)Ut(theta,n,alpha,(nargin==2)),stepsz,Nleap);
%     [theta_til,u,du,acpt_ind]=cSphHMC(theta_til,u,du,@(theta,der)Uc_full(theta,n,alpha,(nargin==2)),stepsz,Nleap);
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        disp('Burn in completed!');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        theta=theta_til(1:end-1);
        samp(iter-NBurnIn,:)=theta.^2;
%         nom2_theta = theta'*theta;
%         nom2oinf = sqrt(nom2_theta)/max(abs(theta)); % ratio of 2 norm vs infinite norm of theta
%         % tansform to unit cube
%         x_c = theta.*nom2oinf;
%         z = (x_c+1)/2;
%         % transform back to original domain (simplex)
%         p = c2x(z);
%         samp(iter-NBurnIn,:)=p;
        acpt=acpt+acpt_ind;
        wt(iter-NBurnIn)=sum(log(abs(theta_til))) + (D-1)*log(2); % log weights
%         wt(iter-NBurnIn)=(D-1)*log(nom2oinf)+log(abs(theta_til(end))) - (D-1)*log(2) + sum(log(p(1:end-1)./z));
    end

end

% save result
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% thinning
samp=samp(1:thin:(Nsamp-NBurnIn),:);
wt=wt(1:thin:(Nsamp-NBurnIn));
wt=exp(wt-median(wt)); % recover weights
% resample
resamp_idx=datasample(1:size(samp,1),floor((Nsamp-NBurnIn)/thin),'replace',true,'weights',wt);
curTime=regexprep(num2str(fix(clock)),'    ','_');
curfile=['MD_SphHMC_step',num2str(stepsz,'%.0e'),'_',curTime];
save(['result/',curfile,'.mat'],'Nsamp','NBurnIn','thin','stepsz','Nleap','samp','wt','resamp_idx','acpt','time');
disp(' ');
disp(['The final acceptance rate is: ',num2str(acpt)]);
disp(' ');
% efficiency measurement
addpath('./result/');
CalculateStatistics(curfile,'./result/');

% Energy function of MD distribution %
% user defined. p is the full probability vector %

function [u,du]=UdU(p,n,alpha)

u = -(n+alpha-1)'*log(p);

if nargin>1
    du = -(n+alpha-1)./p;
end

end
#!/Users/LANZI/anaconda/bin/python


import argparse
import itertools
import numpy as np
import os

import processwiki as pw
import samplers

home = os.path.expanduser('~')
np.set_printoptions(precision=3, suppress=True)
np.random.seed(2015)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('algNO', nargs='?', type=int, default=0)
    parser.add_argument('alpha', nargs='?', type=float, default=0.01)
    parser.add_argument('beta', nargs='?', type=float, default=0.5)
    parser.add_argument('K', nargs='?', type=int, default=100)
    parser.add_argument('batch_size', nargs='?', type=int, default=50)
    parser.add_argument('epsilon', nargs='?', type=float, default=0.01)
    parser.add_argument('tau0', nargs='?', type=float, default=1000)
    parser.add_argument('kappa', nargs='?', type=float, default=0.6)
    parser.add_argument('samples_per_update', nargs='?', type=int, default=100)
    parser.add_argument('num_updates', nargs='?', type=int, default=1000)
    parser.add_argument('output_dir', nargs='?', type=str, default='.')
    parser.add_argument('algs', nargs='?', type=str, default=('RLD','wallLMC','sphHMC','sphLMC'))
    args = parser.parse_args()

    D = 3.3e6 # Approx number of documents in wikipedia

    vocab = pw.create_vocab('wiki.vocab')
    W = len(vocab)

    # Data is a list of form [(article name, article word cts)]
    #data = pw.parse_docs(pw.online_wiki_docs(), vocab)
    # Can save data to a file like this
    #pw.dump_list(itertools.islice(data, 1000), 'stored.pkl')
    #pw.save_data(itertools.islice(data, args.samples_per_update*args.num_updates), 'stored.pkl')
    # And load like this
    data = pw.load_list('stored_100K.pkl')

    # Use first 1000 docs as held out test set
    test_data = itertools.islice(data, 1000)
    (test_names, holdout_train_cts, holdout_test_cts) = list(zip(*list(test_data)))
    holdout_train_cts = list(holdout_train_cts)
    holdout_train_cts = list(holdout_train_cts)

    batched_cts = pw.take_every(args.batch_size, data)

    # initialization
    if args.algNO==0:
        theta0 = np.random.gamma(1,1,(args.K,W))
    else:
        phi0 = np.random.uniform(size=(args.K,W))
        phi0 = phi0/np.sum(phi0,1)[:,np.newaxis]
        if args.algNO==1:
            theta0 = phi0[:,:-1]
            args.epsilon=.2;args.kappa=2.2
        if args.algNO in [2,3]:
            theta0 = np.sqrt(phi0)
            if args.algNO==2:
                args.epsilon=.1;args.kappa=1
            if args.algNO==3:
                args.epsilon=.2;args.kappa=1.5
    
    step_size_params = (args.epsilon, args.tau0, args.kappa)
    
    print("Running %s Sampler, epsilon = %g, samples per update = %d" % (args.algs[args.algNO],
            args.epsilon, args.samples_per_update))
    
    sgld_func = samplers.sgSampler(D, args.K, W, args.alpha, args.beta, theta0,
            step_size_params, args.output_dir, args.algs[args.algNO]).run_online
    sgld_args = (args.num_updates, args.samples_per_update, batched_cts,
            holdout_train_cts, holdout_test_cts)
    sgld_func(*sgld_args)


if __name__ == '__main__':
    main()

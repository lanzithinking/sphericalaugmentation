Stochastic Gradient Spherical Lagrangian Monte Carlo for Latent Dirichlet Allocation

Shiwei Lan and Babak Shahbaba, 2015

This is modified from

‘Stochastic Gradient Riemannian Langevin Dynamics for Latent Dirichlet Allocation

Sam Patterson and Yee Whye Teh, 2013’


The codes included can be run in Python3. samplers.py includes 4 algorithms: 1.RLD, 2.wallLMC, 3.sphHMC and 4.sphLMC discussed in

Sampling constrained probability distributions using Spherical Augmentation
http://arxiv.org/abs/1506.05936

An example run to call python code to run LDA with sphLMC update:

python3 run_wiki.py 4


Please include proper reference when cited, thanks!

07-06-2015
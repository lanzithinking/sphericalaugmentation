

import errno
import numpy as np
import os
import time
import util_funcs

class sgSampler(object):
    """Stochastic Gradient Monte Carlo algorithms"""
    def __init__(self, D, K, W, alpha, beta, theta0,
            step_size_params, results_dir, alg_name):
        self.D = D
        self.K = K
        self.W = W
        self.alpha = alpha
        self.beta = beta
        self.step_size_params = step_size_params
        self.update_ct = 0
        self.theta = theta0
        phi_ini = {'RLD' : theta0 / np.sum(theta0,1)[:,np.newaxis],
                   'wallLMC' : np.append(theta0,np.array(1-np.sum(theta0,1))[:,np.newaxis],axis=1)}
        phi_ini.update(dict.fromkeys(['sphHMC','sphLMC'], theta0**2))
        self.phi = phi_ini[alg_name]
        
        self.results_dir = results_dir
        self.alg_name = alg_name
        self.time_ct = 0

    def sample_counts(self, train_cts, batch_D, num_samples):
        batch_N = sum(sum(ddict.values()) for ddict in train_cts)
        uni_rvs = np.random.uniform(size = (batch_N)*(num_samples+1))
        z  = [{} for d in range(0, batch_D)]
        Adk = np.zeros((batch_D, self.K), dtype = np.uint32)
        Bkw = np.zeros((self.K, self.W), dtype = np.uint32)
        Adk_mean = np.zeros(Adk.shape)
        Bkw_mean = np.zeros(Bkw.shape)
        burn_in = num_samples // 2
        util_funcs.sample_z_ids(Adk_mean, Bkw_mean, Adk, Bkw, self.phi,
                uni_rvs, train_cts, z, self.alpha, num_samples, burn_in)
        return (Adk_mean, Bkw_mean)
    
    
    # sgRLD using the expanded-mean parameterization #
    def update_RLD(self, train_cts, num_samples):
        batch_D = len(train_cts)
        Adk_mean, Bkw_mean = self.sample_counts(train_cts, batch_D, num_samples)
        phi_star = self.phi.copy()
        theta_star = self.theta.copy()
        (a,b,c) = self.step_size_params
        eps_t  = a*((1 + self.update_ct/b)**-c)
        for k in range(self.K):
            phi_k = self.phi[k,:]
            theta_k = self.theta[k,:]
            z = np.random.randn(self.W)
            # Update theta according to Equation 11 in paper;
            grad = self.beta - theta_k + (self.D/batch_D)*(Bkw_mean[k,:] - np.sum(Bkw_mean[k,:])*phi_k)
            theta_k = np.abs(theta_k + eps_t*grad + (2*eps_t)**.5*z*theta_k**.5)
            theta_star[k,:] = theta_k
            phi_star[k,:] = theta_k / np.sum(theta_k)
        self.phi = phi_star
        self.theta = theta_star
        self.update_ct += 1
        return (Adk_mean, Bkw_mean)

    # stochastic gradient Wall Lagrangian Monte Carlo #
    def update_wallLMC(self, train_cts, num_samples):
        batch_D = len(train_cts)
        Adk_mean, Bkw_mean = self.sample_counts(train_cts, batch_D, num_samples)
        phi_star = self.phi.copy()
        theta_star = self.theta.copy()
        (a,b,c) = self.step_size_params
        eps_t  = a*((1 + self.update_ct/b)**-c)
        for k in range(self.K):
            # initialize
            phi_k = self.phi[k,:]
            theta_k = self.theta[k,:]
            avgn = (self.D/batch_D)*Bkw_mean[k,:]
            estN = max(1,np.sum(avgn))
            # sample velocity
            z = np.random.randn(self.W)
            rtphi = np.sqrt(phi_k)
            v = (rtphi*z - phi_k*(rtphi.dot(z)))/np.sqrt(estN)
            v = v[:-1]
            # update velocity
            n_til = self.beta-1+avgn +1/2
            grad = (n_til-phi_k*np.sum(n_til))/estN
            grad = grad[:-1]
            v += eps_t/2*grad
            # update theta
            theta0 = theta_k
            theta_k = theta_k + eps_t*v
            # handle constraints by wall hitting
            while np.sum(np.abs(theta_k))>1:
                if all(theta0*theta_k>=0):
                    ndirxn = np.sign(theta0)
                else:
                    no0_idx = (theta_k-theta0!=0)
                    xT = theta0[no0_idx]/(theta0[no0_idx]-theta_k[no0_idx])
                    xT = np.sort(xT[(xT>=0)&(xT<=1)])
                    xT = np.hstack((0,xT,1))
                    intsct = theta0[:,np.newaxis]+np.mat(theta_k-theta0).T*np.mat(xT)
                    #intsct[np.abs(intsct)<1e-10] = 0
                    firstoff = 1+np.min(np.nonzero(np.sum(np.abs(intsct[:,1:]),0).A1>1))
                    ndirxn = np.sign(np.sum(np.sign(intsct[:,firstoff-1:firstoff+1]),1)).A1
                    
                hitime = (1-ndirxn.dot(theta0))/ndirxn.dot(theta_k-theta0)
                theta0 = theta0 + (theta_k-theta0)*hitime
                theta_k = theta_k - 2*ndirxn*(ndirxn.dot(theta_k)-1)/(self.W-1)
            
            theta_star[k,:] = theta_k # always accept
            phi_star[k,:] = np.append(np.abs(theta_k),1-sum(np.abs(theta_k)))
        self.phi = phi_star
        self.theta = theta_star
        self.update_ct += 1
        return (Adk_mean, Bkw_mean)
    
    # stochastic gradient Spherical Hamiltonian Monte Carlo #
    def update_sphHMC(self, train_cts, num_samples):
        batch_D = len(train_cts)
        Adk_mean, Bkw_mean = self.sample_counts(train_cts, batch_D, num_samples)
        phi_star = self.phi.copy()
        theta_star = self.theta.copy()
        (a,b,c) = self.step_size_params
        eps_t  = a*((1 + self.update_ct/b)**-c)
        for k in range(self.K):
            # initialize
            #phi_k = self.phi[k,:]
            theta_k = self.theta[k,:]
            # sample velocity
            v = np.random.randn(self.W)
            v -= theta_k*np.dot(theta_k,v) # project to tangent plane
            # update velocity
            n_til = self.beta-1+(self.D/batch_D)*Bkw_mean[k,:] +1/2
            grad = 2*(n_til/theta_k-theta_k*np.sum(n_til))
            v += eps_t/2*grad
            # Update theta along great circle;
            v_nom = v.dot(v)**.5
            cosvt = np.cos(eps_t*v_nom)
            sinvt = np.sin(eps_t*v_nom)
            theta_k = theta_k*cosvt+v/v_nom*sinvt
            theta_star[k,:] = theta_k # always accept
            phi_star[k,:] = theta_k**2
        
        self.phi = phi_star
        self.theta = theta_star
        self.update_ct += 1
        return (Adk_mean, Bkw_mean)
    
    # stochastic gradient Spherical Lagrangian Monte Carlo #
    def update_sphLMC(self, train_cts, num_samples):
        batch_D = len(train_cts)
        Adk_mean, Bkw_mean = self.sample_counts(train_cts, batch_D, num_samples)
        phi_star = self.phi.copy()
        theta_star = self.theta.copy()
        (a,b,c) = self.step_size_params
        eps_t  = a*((1 + self.update_ct/b)**-c)
        for k in range(self.K):
            # initialize
            #phi_k = self.phi[k,:]
            theta_k = self.theta[k,:]
            avgn = (self.D/batch_D)*Bkw_mean[k,:]
            estN = max(1,np.sum(avgn))
            # sample velocity
            v = np.random.randn(self.W)
            v -= theta_k*(theta_k.dot(v)) # project to tangent plane
            v /= 2*np.sqrt(estN)
            # update velocity
            n_til = self.beta-1+avgn +1/2
            grad = (n_til/theta_k-theta_k*np.sum(n_til))/(2*estN)
            v += eps_t/2*grad
            # Update theta along great circle;
            v_nom = v.dot(v)**.5
            cosvt = np.cos(eps_t*v_nom)
            sinvt = np.sin(eps_t*v_nom)
            theta_k = theta_k*cosvt+v/v_nom*sinvt
            theta_star[k,:] = theta_k # always accept
            phi_star[k,:] = theta_k**2
        
        self.phi = phi_star
        self.theta = theta_star
        self.update_ct += 1
        return (Adk_mean, Bkw_mean)
    
    def update(self, train_cts, num_samples):
        method_name = 'update_' + str(self.alg_name)
        try:
            update_method = getattr(self, method_name)
        except AtributeError:
            print(alg_name, 'not found!')
        else:
            return update_method(train_cts, num_samples)
    
    def run_online(self, num_updates, samples_per_update, batched_cts,
            holdout_train_cts, holdout_test_cts):
        self.log_preds = []
        t = 1
        self.ho_log_preds = []
        self.avg_probs = {(d, w): 0.0 for (d, ctr) in enumerate(holdout_test_cts) for w in ctr}
        self.ho_count = 0
        self.create_output_dir()
        (a,b,c) = self.step_size_params
        params_dict = {'a':a,'b':b,'c':c,'alpha':self.alpha,'beta':self.beta,
                'K':self.K,'samples':samples_per_update,'func':'online'}
        sampler_name = self.__class__.__name__+'_'+self.alg_name
        for batch in batched_cts:            
            params_dict['batch_size'] = len(batch)
            self.basename = str(params_dict)
            (names, train_cts, test_cts) = list(zip(*batch))
            train_cts = list(train_cts)
            test_cts = list(test_cts)
            start = time.time()
            (Adk_mean, Bkw_mean) = self.update(train_cts, samples_per_update)
            self.log_preds.append(self.log_pred(test_cts, Adk_mean,Bkw_mean))
            end = time.time()
            time_this_update = end - start
            self.time_ct += time_this_update
            docs_so_far = t * len(batch)
            print(sampler_name + " iteration %d, docs so far %d, log pred %g, time %g" % (t, docs_so_far, self.log_preds[t-1], time_this_update))
            #self.store_phi(t)
            # Assess holdout log-pred every 5000 documents
            if docs_so_far % 5000  == 0:
                ho_lp = self.holdout_log_pred(holdout_train_cts, holdout_test_cts, samples_per_update)
                self.ho_log_preds.append([docs_so_far, ho_lp, self.time_ct])
                self.save_variables(['ho_log_preds'])
                #self.save_stored_phis()
            if t == num_updates:
                break
            t += 1

    def log_pred(self, test_cts, Adk_mean, Bkw_mean):
        eta_hat = Adk_mean + self.alpha
        eta_hat /= np.sum(eta_hat, 1)[:, np.newaxis]
        phi_hat = self.phi
        log_probs = {(d, w): cntr[w]*np.log(np.dot(eta_hat[d, :],
                     phi_hat[:, w])) for (d, cntr) in enumerate(test_cts)
                     for w in cntr}
        num_words = sum(sum(cntr.values()) for cntr in test_cts)
        return sum(log_probs.values()) / num_words

    def holdout_log_pred(self, holdout_train_cts, holdout_test_cts, num_samps):
        batch_D = len(holdout_train_cts)
        Adk_mean, Bkw_mean = self.sample_counts(holdout_train_cts, batch_D,
                                                num_samps)
        eta_hat = Adk_mean + self.alpha
        eta_hat /= np.sum(eta_hat, 1)[:, np.newaxis]
        phi_hat = self.phi
        T = self.ho_count
        old_avg = self.avg_probs
        avg_probs = {(d, w): (T*old_avg[(d, w)] +
                              np.dot(eta_hat[d, :], phi_hat[:, w])) / (T+1)
                     for (d, w) in old_avg}
        self.avg_probs = avg_probs
        self.ho_count += 1
        log_avg_probs = {(d, w): cntr[w] * np.log(avg_probs[(d, w)])
                         for (d, cntr) in enumerate(holdout_test_cts) for w in cntr}
        num_words = sum(sum(cntr.values()) for cntr in holdout_test_cts)
        return sum(log_avg_probs.values()) / num_words

    def create_output_dir(self):
        self.dirname = os.path.join(self.results_dir, 'result')
        try:
            os.makedirs(self.dirname)
        except OSError as exc:
            if exc.errno == errno.EEXIST:
                pass
            else:
                raise

    def save_variables(self, attrs):
        for attr in attrs:
            np.savetxt(os.path.join(self.dirname,
                                    self.basename+attr+'_'+self.alg_name+'.dat'),
                       np.array(getattr(self, attr)))

    def store_phi(self, iter_num):
        try:
            self.stored_phis[str(iter_num)] = self.phi
        except AttributeError:
            self.stored_phis = {str(iter_num): self.phi}

    def save_stored_phis(self):
        np.savez(os.path.join(self.dirname, self.basename+'_stored_phis.npz'),
                 **self.stored_phis)

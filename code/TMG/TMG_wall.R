## Simulation of Truncated Multivariate Gaussian with wall HMC ##

rm(list=ls())
set.seed(2013)
#library(mvtnorm)
#library(gtools)
#library(MASS)

## data
D = 100
Sigma = diag(rep(1,D))
for(d in 1:(D-1)) Sigma[row(Sigma)==col(Sigma)+d] = 1/(d+1)
Sigma[upper.tri(Sigma)] = t(Sigma)[upper.tri(Sigma)]
lo = rep(0,D)
up = c(5,rep(.5,D-1))
#up = 10/(1:D)

## likelihood functions needed
U = function(x,d=F){ # potential and its gradient in x
	dloglik = -solve(Sigma,x)
	if(d==F){
	    loglik = x%*%dloglik/2
	    return(-loglik)
    }else if(d==T){
	    return(-dloglik)
    }else stop('Wrong choice!')
}

source('../sampler/WallHMC.R')

## HMC setting
TrjLength = 1
NLeap = 5
Stepsz = TrjLength/NLeap
# step size for D=10,100, up1: 1/2, 1/5
# step size for D=10,100, up2: 1/2, 1/4

## storage
NSamp = 110000
NBurnIn = 10000
Samp = matrix(NA,NSamp-NBurnIn,D)
acpt = 0 # overall acceptance
accp = 0 # online acceptance
wallhits = 0 # count the number of Wall hitting

## Initialization
x = runif(D)
u = U(x); du = U(x,T)

cat('Running Wall HMC...\n')
start = proc.time()
for(Iter in 1:NSamp){
	
    # display online acceptance rate per 100 iteration
    if(Iter%%100==0){
        cat('Iteration ',Iter,' completed!\n')
        cat('Acceptance Rate: ', accp/100,'\n')
        accp=0
    }
    
    # Use Wall HMC to get sample x
    samp = WallHMC(x, u, du, function(x,d=F)U(x,d),lo,up, Stepsz, NLeap)
    x = samp$q
	u = samp$u; du = samp$du
    accp = accp + samp$Ind
    
    if(Iter==NBurnIn) cat('Burn in completed!\n')
    
    # save sample beta
    if(Iter>NBurnIn){
        Samp[Iter-NBurnIn,] = x
        acpt = acpt + samp$Ind
		wallhits = wallhits + samp$wallhit
    }
    
}
Time = proc.time()-start
Time = Time[1]

# Final Acceptance Rate
acpt = acpt/(NSamp-NBurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')
hitrate = wallhits/(NSamp-NBurnIn)
cat('The Wall hitting Rate: ',hitrate,'\n')


## Save samples to file
save(NSamp,NBurnIn,TrjLength,NLeap,Stepsz,Samp,hitrate,acpt,Time,file=paste('./result/TMG_wall_D',D,'_',Sys.time(),'.RData',sep=''))

## plot all sample
#pdf(paste('./result/TMG_wall_D',D,'_',Sys.time(),'.pdf',sep=''))
#op = par(mfrow=c(2,2),mar=c(3,3,2,1) + 0.1,oma=rep(0,4),mgp=c(2,1,0))
#
##matplot(Samp,type='l')
#plot(Samp[,1],Samp[,2],pch=16,xlab=expression(x[1]),ylab=expression(x[2]))
#image(kde2d(Samp[,1],Samp[,2],n=50),xlab=expression(x[1]),ylab=expression(x[2]))
#sqx1 = seq(0,5,length=50); sqx2 = seq(0,1,length=50)
#bivn.str = outer(sqx1,sqx2,function(x1,x2)dmvnorm(cbind(x1,x2),sigma=Sigma))
#contour(sqx1,sqx2,bivn.str,drawlabels=F,add=T)
##contour(kde2d(Samp[,1],Samp[,2],n=20),lty=2,add=T,drawlabels=F);
#
#
#hx1=hist(Samp[,1],50,probability=T,xlab=expression(x[1]),main=NULL)
##lines(hx1$mids,hx1$density,lty=2)
#curve(2*dnorm(x),from=0,add=T)
##legend('topright',legend=c('True','Estimate'),lty=1:2)
#
#hx2=hist(Samp[,2],50,probability=T,xlab=expression(x[2]),main=NULL)
##lines(hx2$mids,hx2$density,lty=2)
#curve(dnorm(x)/(pnorm(u[2])-pnorm(l[2])),from=0,to=1,add=T)
##legend('topright',legend=c('True','Estimate'),lty=1:2)
#
#par(op)
#dev.off()










## Simulation of Truncated Multivariate Gaussian with wall HMC ##

TMG_wall = function(SEED,NSamp=11000,NBurnIn=1000){
set.seed(SEED)

## data
D = 2
Sigma = diag(rep(1,D))
for(d in 1:(D-1)) Sigma[row(Sigma)==col(Sigma)+d] = 1/(d+1)
Sigma[upper.tri(Sigma)] = t(Sigma)[upper.tri(Sigma)]
lo = rep(0,D)
up = c(5,rep(1,D-1))

## likelihood functions needed
U = function(x,d=F){ # potential and its gradient in x
	dloglik = -solve(Sigma,x)
	if(d==F){
		loglik = x%*%dloglik/2
		return(-loglik)
	}else if(d==T){
		return(-dloglik)
	}else stop('Wrong choice!')
}

source('../../sampler/WallHMC.R')

## HMC setting
TrjLength = 2*pi/D
NLeap = 5
Stepsz = TrjLength/NLeap

## storage
Samp = matrix(NA,NSamp-NBurnIn,D)
acpt = 0 # overall acceptance
accp = 0 # online acceptance
wallhits = 0 # count the number of Wall hitting

## Initialization
x = runif(D)
u = U(x); du = U(x,T)

cat('Running Wall HMC...\n')
start = proc.time()
for(Iter in 1:NSamp){
	
    # display online acceptance rate per 100 iteration
    if(Iter%%100==0){
        cat('Iteration ',Iter,' completed!\n')
        cat('Acceptance Rate: ', accp/100,'\n')
        accp=0
    }
    
    # Use Wall HMC to get sample theta
    samp = WallHMC(x, u, du, function(x,d=F)U(x,d),lo,up, Stepsz, NLeap)
    x = samp$q
	u = samp$u; du = samp$du
    accp = accp + samp$Ind
    
    if(Iter==NBurnIn) cat('Burn in completed!\n')
    
    # save sample beta
    if(Iter>NBurnIn){
        Samp[Iter-NBurnIn,] = x
        acpt = acpt + samp$Ind
		wallhits = wallhits + samp$wallhit
    }
    
}
Time = proc.time()-start
Time = Time[1]

# Final Acceptance Rate
acpt = acpt/(NSamp-NBurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')
hitrate = wallhits/(NSamp-NBurnIn)
cat('The Wall hitting Rate: ',hitrate,'\n')


## Save samples to file
#save(NSamp,NBurnIn,TrjLength,NLeap,Stepsz,Samp,hitrate,acpt,Time,file=paste('./result/TMG_wall_D',D,'_',Sys.time(),'.RData',sep=''))
return(Samp)

}








## Simulation of Truncated Multivariate Gaussian with sc-SphHMC ##

TMG_scSph = function(SEED,NSamp=11000,NBurnIn=1000){
set.seed(SEED)

## data
D = 2
Sigma = diag(rep(1,D))
for(d in 1:(D-1)) Sigma[row(Sigma)==col(Sigma)+d] = 1/(d+1)
Sigma[upper.tri(Sigma)] = t(Sigma)[upper.tri(Sigma)]
lo = rep(0,D)
up = c(5,rep(1,D-1))

## likelihood functions needed
U = function(x,d=F){ # potential and its gradient in x, user defined
	dloglik = -solve(Sigma,x)
	if(d==F){
		loglik = x%*%dloglik/2
		return(-loglik)
	}else if(d==T){
		return(-dloglik)
	}else stop('Wrong choice!')
}

Ut = function(theta,lo=lo,up=up,d=F){ # potential and its gradient in theta (theta in (0,pi)^(D-1)*(0,2*pi)) after adjusting Jacobian
	D = length(theta);
	J_xtheta = (up-lo)/(c(rep(1,D-1),2)*pi)
	x = lo+theta*J_xtheta
	if(d==F){
		ut = U(x)
		return(ut)
	}else if(d==T){
		du = U(x,T); dut = du*J_xtheta
		return(dut)
	}else stop('Wrong choice!')
}

source('../../sampler/scSphHMC_unitr.R')

## HMC setting
TrjLength = 2*pi/D
NLeap = 5
Stepsz = TrjLength/NLeap

## storage
Samp = matrix(NA,NSamp-NBurnIn,D)
acpt = 0 # overall acceptance
accp = 0 # online acceptance
wt = rep(NA,NSamp-NBurnIn)

## Initialization
x = lo+up*runif(D)
theta = pi*c(rep(1,D-1),2)/(up-lo)*(x-lo) # theta in (0,pi)^(D-1)*(0,2*pi)
u = Ut(theta,lo,up); du = Ut(theta,lo,up,T)

cat('Running Spherical HMC in spherical coordinate...\n')
start = proc.time()
for(Iter in 1:NSamp){
	
    # display online acceptance rate per 100 iteration
    if(Iter%%100==0){
        cat('Iteration ',Iter,' completed!\n')
        cat('Acceptance Rate: ', accp/100,'\n')
        accp=0
    }
    
    # Use Spherical HMC to get sample theta
    samp = SphHMC(theta, u, du, function(theta,d=F)Ut(theta,lo,up,d), Stepsz, NLeap)
    theta = samp$q
	u = samp$u; du = samp$du
    accp = accp + samp$Ind
    
    if(Iter==NBurnIn) cat('Burn in completed!\n')
    
    # save sample beta
    if(Iter>NBurnIn){
		J_xtheta = (up-lo)/(c(rep(1,D-1),2)*pi)
		x = lo+theta*J_xtheta # back to constrained domain
        Samp[Iter-NBurnIn,] = x
        acpt = acpt + samp$Ind
#		wt[Iter-NBurnIn] = prod(((up-lo))/(pi*c(rep(1,D-1),2)))*exp((1:(D-1)-D)%*%log(sin(theta[-D]))) # weights
		wt[Iter-NBurnIn] = (1:(D-1)-D)%*%log(sin(theta[-D])) + sum(log(J_xtheta)) # log weights
    }
    
}
Time = proc.time()-start
Time = Time[1]

# Final Acceptance Rate
acpt = acpt/(NSamp-NBurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')

# Resample
wt = exp(wt-median(wt)) # recover the weights
ReSamp_idx = sample(NSamp-NBurnIn,NSamp-NBurnIn,replace=T,prob=wt)
ReSamp = Samp[ReSamp_idx,]

## Save samples to file
#save(NSamp,NBurnIn,TrjLength,NLeap,Stepsz,Samp,wt,ReSamp_idx,acpt,Time,file=paste('./result/TMG_scSph_D',D,'_',Sys.time(),'.RData',sep=''))
return(ReSamp)

}









